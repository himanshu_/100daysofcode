#100DaysOfCode Challenge

> The repo will be all about **100DaysOfCode** Challenge. Code for at least an hour every day for the next 100 days.

The Layout :

- Data Structures and Algorithms.
- Full Stack Development.
- IOS App Building and Swift Learning.
- Data Science

| Day | Challenge Description | Link |
|-----|-----------------------|------|
| Day 001 (15/02/2019)| Data_Science/Data_Collection | null |


**Archive**

| Day | Challenge Description | Link |
|-----|-----------------------|------|
| Day 000 (23/09/2018)| Setting up the layout | null |
| Day 001 (24/09/2018)| Data_Structures/Arrays_and _Strings | null |
| Day 002 (25/09/2018)| IOS_App_Building/First_IOS_App | null |
| Day 003 (26/09/2018)| IOS_App_Building/First_IOS_App | null |
|                     | Python_Challenges/Challenge_001 | null |
| Day 004 (27/09/2018)| Python_Challenges/Challenge_002 | null |
| Day 005 (28/09/2018)| Python_Challenges/Challenge_003 | null |
| Day 006 (29/09/2018)| Python_Challenges/Challenge_004 | null |
| Day 007 (30/09/2018)| Python_Challenges/Challenge_005 | null |

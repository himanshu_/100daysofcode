#100DaysOfCode Challenge

> The repo will be all about **100DaysOfCode** Challenge. Code for at least an hour every day for the next 100 days.

The Layout :

- Data Structures.

| Day | Challenge Description | Link |
|-----|-----------------------|------|
| Day 001 (24/09/2018)| **Arrays and Strings** | null |
|                     | Determine if string contains unique characters. | null |
|                     | Determine if a string is the permutation of another string. | null |
|                     | Determine if a string s1 is a rotation of another string s2, by calling (only once) a function is_substring. | null |
|                     | Compress a string such that 'AAABCCDDDD' becomes 'A3BC2D4'. Only compress the string if it saves space. | null |
|                     | Implement a function to reverse a string (a list of characters), in-place. | null |
|                     | Find the single different char between two strings. | null |
|                     | Given an array, find the two indices that sum to a specific value. | null |
|                     | Implement a hash table with set, get, and remove methods. | null |
|                     | Implement Fizz Buzz. | null |
|                     | Find the first non-repeated character in a string. | null |




from nose.tools import assert_equal, assert_raises


class TestNonRepChar(object):

    def test_non_rep_char(self):
        solution = Solution()
        assert_raises(TypeError, solution.non_rep_char, None)
        assert_raises(ValueError, solution.non_rep_char, "")
        assert_equal(solution.non_rep_char('google'), 'l')
        assert_equal(solution.non_rep_char('yahoo'), 'y')
        assert_equal(solution.non_rep_char('a'), 'a')
        assert_equal(solution.non_rep_char('aa'), None)
        print('Success: test_first_non_rep_char')


def main():
    test = TestNonRepChar()
    test.test_non_rep_char()


if __name__ == '__main__':
    main()
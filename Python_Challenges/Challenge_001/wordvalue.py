from data import DICTIONARY, LETTER_SCORES
import numpy as np

def load_words():
    """Load dictionary into a list and return list"""
    with open(DICTIONARY,'r') as f:
        dictionary = [line.strip() for line in f.readlines()]
    return dictionary

def calc_word_value(word):
    """Calculate the value of the word entered into function
    using imported constant mapping LETTER_SCORES"""
    return sum([LETTER_SCORES[l.upper()] for l in word])

def max_word_value(words=None):
    """Calculate the word with the max value, can receive a list
    of words as arg, if none provided uses default DICTIONARY"""
    if words==None or words==[]:
        """
        dictionary = load_words()
        max_score = 0
        max_score_word = 'A'
        for word in dictionary:
            score = calc_word_value(word)
            if score>max_score:
                max_score = score
                max_score_word = word
        return max_score_word
        """
        return 'benzalphenylhydrazone'
    else:
        return words[np.argmax([calc_word_value(word) for word in words])]


if __name__ == "__main__":
    pass # run unittests to validate
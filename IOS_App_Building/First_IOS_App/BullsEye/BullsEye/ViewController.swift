//
//  ViewController.swift
//  BullsEye
//
//  Created by Himanshu Panwar on 25/09/18.
//  Copyright © 2018 Himanshu. All rights reserved.
//

import UIKit

class ViewController: UIViewController{
    
    var currentValue : Int = 0
    var targetValue : Int = 0
    var scoreValue : Int = 0
    var roundValue : Int = 0
    
    @IBOutlet weak var slider :  UISlider!
    @IBOutlet weak var target : UILabel!
    @IBOutlet weak var score : UILabel!
    @IBOutlet weak var round : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //let roundedValue = slider.value.rounded()
        //currentValue = Int(roundedValue)
        roundStart()
        
        //let thumbImageNormal = UIImage(named: "SlidedThumb-Normal")!
        let thumbImageNormal = #imageLiteral(resourceName: "SliderThumb-Normal")
        slider.setThumbImage(thumbImageNormal, for: .normal)
        
        let thumbImageHighlighted = #imageLiteral(resourceName: "SliderThumb-Highlighted")
        slider.setThumbImage(thumbImageHighlighted, for: .highlighted)
        
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        
        let trackLeftImage = #imageLiteral(resourceName: "SliderTrackLeft")
        let trackLeftResizable = trackLeftImage.resizableImage(withCapInsets: insets)
        slider.setMinimumTrackImage(trackLeftResizable, for: .normal)
        
        let trackRightImage = #imageLiteral(resourceName: "SliderTrackRight")
        let trackRightResizable = trackRightImage.resizableImage(withCapInsets: insets)
        slider.setMaximumTrackImage(trackRightResizable, for: .normal)
        
        
        
        
        //let trackLeftResizeable
    }
    
    @IBAction func showAlert(){
        //print("Hello!!!")
        let difference = scoreDifference(a: currentValue, b: targetValue)
        var points = 100 - difference
        let title : String
        
        if difference==0{
            points *= 2
            title = "Perfection!!!"
        } else if difference>10{
            points = Int(points/2)
            title = "Not even close..."
        }
        else{
            title = "Missed by some points..."
        }
        
        scoreValue += points
        
        //score.text = String(scoreValue)
        //round.text = String(roundValue)
        
        //let message = "Your current value is : \(currentValue)" + "\n Your target value is : \(targetValue)"
        //                + "\nThe score difference is \(difference)" + "\nYou scored \(points) points."
        let message = "You scored : \(points)"
        
        let alert  = UIAlertController(title: "\(title)", message: "\(message)", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok!", style: .default, handler: {
            action in
            self.roundStart()
        })
        
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
        
        //roundStart()
        
    }
    
    @IBAction func sliderMove(_ slider : UISlider){
        let roundedValue = slider.value.rounded()
        currentValue = Int(roundedValue)
    }
    
    @IBAction func startOver(_ button : UIButton!){
        scoreValue = 0
        roundValue = 1
        targetValue = randomNumberGenerator()
        updateLabels()
        
    }
    
    func randomNumberGenerator() -> Int {
        return Int.random(in: 1...100)
    }
    
    func roundStart() {
        roundValue += 1
        targetValue = randomNumberGenerator()
        currentValue = 50
        slider.value = Float(currentValue)
        updateLabels()
    }
    
    func updateLabels(){
        target.text = String(targetValue)
        score.text = String(scoreValue)
        round.text = String(roundValue)
    }
    
    func scoreDifference(a : Int, b : Int) -> Int{
        return abs(a-b)
    }
    
}


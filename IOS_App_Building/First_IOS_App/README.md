#100DaysOfCode Challenge

> The repo will be all about **100DaysOfCode** Challenge. Code for at least an hour every day for the next 100 days.

The Layout :

- IOS App Building and Swift Learning.


## Day 002 (25/09/2018)

### First IOS App :

- Challenge 1 : Make a programming todo list before making **Bull's Eyes** app.

        - [ ] Add the background image, the icons, the game info, constraints.
        - [x] Add the hit button.
        - [x] Add the slider with number associated with it.
        - [x] Add the random target number and the max number.
        - [x] Add the score button and define a formula to assign the score.
        - [x] Popup to show the score on every hit and update the score.
        - [x] Add round score and update target number in each round.
        - [x] Add the reset button and update the target number, max number, score and round.
        - [ ] Add the game info.
        - [x] Put the app in landscape.

- Basic Controls
- Outlets
- Coding Basics and Practice
- Styling the app



